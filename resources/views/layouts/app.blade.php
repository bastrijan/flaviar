<!DOCTYPE html>
<html lang="en">
<head>
   
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Flaviar</title>

    <!-- Styles -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" />
    <!--
    <link rel="stylesheet" href="/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="/css/responsive.dataTables.min.css" />
    -->
    <link rel="stylesheet" href="/css/datatables.css" />

    <link rel="stylesheet" href="/css/sweetalert.css" />
    <link rel="stylesheet" href="/css/bootstrap-switch.min.css" />
    <link rel="stylesheet" href="/css/main.css" />
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css" />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        @yield('content')
    </div>

    <!-- Scripts -->
    <script type="text/javascript" src="/js/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <!--
    <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/js/dataTables.responsive.min.js"></script>
    -->
    <script type="text/javascript" src="/js/datatables.min.js"></script>

    <script type="text/javascript" src="/js/sweetalert.min.js"></script>
    {{--<script type="text/javascript" src="/js/sweetalert2.min.js"></script>--}}
    <script type="text/javascript" src="/js/bootstrap-switch.min.js"></script>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="/vendor/jsvalidation/js/jsvalidation.min.js"></script>

    <!-- ova e za scroll to top, patekata do slikickata se naoga vo samata skripta, ako ja menuvas i tamu treba da se smeni -->
    <script src="/js/back-to-top.js"></script>

    <!-- ova se koristi za cookies -->
    <script type="text/javascript" src="/js/jquery.cookie.js"></script>


    <!-- App scripts -->
    @stack('scripts')

</body>
</html>
