@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="pageTitle"> 
            Assignment 1
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <button class="btn btn-primary" id="bnt-get-rates">Get rates from exchangeratesapi.io</button>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <a href="/export/excel">
                        <button class="btn btn-primary" id="bnt-export-rates">Export to Excel</button>
                    </a>
                </div>
            </div>
        </div>
                        

        <table class="table table-bordered table-striped compact" cellspacing="0" id="rates-table"  style="width: 100%;">
            <thead>
                <tr>
                    <th>Year</th>
                    <th>Month</th>
                    <th>MIN</th>
                    <th>MAX</th>
                    <th>AVG</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
@endsection

@push('scripts')
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script>
$(document).ready(function () {

    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
        "date-range-pre": function ( a ) {
            var monthArr = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            return monthArr.indexOf(a); 
        },
         "date-range-asc": function ( a, b ) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
         "date-range-desc": function ( a, b ) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    } );

    var table = jQuery('#rates-table').DataTable({
        buttons: [],
        pageLength: 25,
        responsive: true,
        processing: true,
        serverSide: false,
        filter: false,
        ajax: "/getfromDbAll",
        columns: [
            { data: 'year', name: 'year' },
            { data: 'month', name: 'month' },
            { data: 'min_rate', name: 'min_rate' },
            { data: 'max_rate', name: 'max_rate' },
            { data: 'avg_rate', name: 'avg_rate' }
        ],
        "columnDefs": [
            { className: "dt-center", targets: [0, 1] },
            { className: "dt-right", targets: [2, 3, 4] },
            { type: 'date-range', targets: 1 }
        ],
   
    });


    jQuery("#bnt-get-rates").click(function(e) {
        e.preventDefault();

        swal({
            title: "Please wait, getting data...",
            imageUrl: "/img/ajax-loading-large.gif",
            imageSize: '200x200',
            showConfirmButton: false
        });

        jQuery.ajax({
            method: "GET",
            url: "/getfromRemoteApi"
        })
        .done(function() {

            jQuery.ajax({
                method: "GET",
                url: "/getfromDbAll"
            })
            .done(function(response) {
                var table = jQuery('#rates-table').DataTable();
                table.clear().draw();
                table.rows.add(response.data).draw().columns.adjust().responsive.recalc();
            })
            .always(function() {
                swal.close();
            });

        });
    });

});
</script>
@endpush
