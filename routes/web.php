<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'RatesController@index');
Route::get('/getfromDbAll', 'RatesController@getfromDbAll');
Route::get('/getfromRemoteApi', 'RatesController@getfromRemoteApi');
Route::get('/export/excel', 'RatesController@export');