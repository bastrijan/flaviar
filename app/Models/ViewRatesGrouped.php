<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewRatesGrouped extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'view_rates_grouped';

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
}
