<?php
namespace App\Http\Controllers;

use App\Models\Rate;
use App\Models\ViewRatesGrouped;
use Yajra\Datatables\Datatables;
use GuzzleHttp\ClientInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class RatesController {
    //set XLSX format currency
    const FORMAT_CURRENCY_USD_SIMPLE = '"$"#,####0.0000_-';

	public function index()
	{
		return view("home");
	}

    public function getfromDbAll()
    {
    	$rates = ViewRatesGrouped::all();
        
        // format some of the data in every row and return Datatables appropriate collection
    	return Datatables::of($rates)
        ->editColumn('month', function ($rate) {
            return date("F", mktime(0, 0, 0, $rate->month, 10));
        })
        ->editColumn('min_rate', function ($rate) {
            return "$".$rate->min_rate;
        })
        ->editColumn('max_rate', function ($rate) {
            return "$".$rate->max_rate;
        })
        ->editColumn('avg_rate', function ($rate) {
            return "$".$rate->avg_rate;
        })
        ->make(true);
    }


    public function getfromRemoteApi(ClientInterface $client)
    {
        //delete all rates
        Rate::truncate();

        //send GET request
        $response = $client->request('GET', 'https://api.exchangeratesapi.io/history?start_at=2020-01-01&end_at=2020-12-31&symbols=USD');
                
        if($response->getStatusCode() == 200) {
            $payload = json_decode($response->getBody());

            foreach($payload->rates AS $date => $rate) {
                $dateArr = explode('-', $date);

                $rate = Rate::create([
                    'year' => $dateArr[0],
                    'month' => $dateArr[1],
                    'day' => $dateArr[2],
                    'full_date' => $date,
                    'rate' => $rate->USD
                ]);
            }
        }
    }

    public function export(Spreadsheet $spreadsheet)
    {
        //get data from DB
        $rates = ViewRatesGrouped::all();

        //Specify the properties for this document
        $spreadsheet->getProperties()
            ->setTitle('Flaviar Exchange rates')
            ->setSubject('Flaviar - Bastrijan Ali')
            ->setDescription('Flaviar - assigment 1')
            ->setCreator('Bastrijan Ali')
            ->setLastModifiedBy('Bastrijan Ali');

        //Adding headers and style to the excel sheet
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Year')
            ->setCellValue('B1', 'Month')
            ->setCellValue('C1', 'MIN')
            ->setCellValue('D1', 'MAX')
            ->setCellValue('E1', 'AVG');

        $spreadsheet->getActiveSheet()
            ->getStyle("A1")->getFont()
            ->setBold(true);

        $spreadsheet->getActiveSheet()
            ->getStyle("B1")->getFont()
            ->setBold(true);

        $spreadsheet->getActiveSheet()
            ->getStyle("C1")->getFont()
            ->setBold(true);

        $spreadsheet->getActiveSheet()
            ->getStyle("D1")->getFont()
            ->setBold(true);

        $spreadsheet->getActiveSheet()
            ->getStyle("E1")->getFont()
            ->setBold(true);

        //adding rate data
        $rowCnt = 2;
        foreach ($rates as $rate) {
            //write data
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'. $rowCnt, $rate->year)
                ->setCellValue('B'. $rowCnt, date("F", mktime(0, 0, 0, $rate->month, 10)))
                ->setCellValue('C'. $rowCnt, $rate->min_rate)
                ->setCellValue('D'. $rowCnt, $rate->max_rate)
                ->setCellValue('E'. $rowCnt, $rate->avg_rate);

            //set USD number currency format
            $spreadsheet->getActiveSheet()
                ->getStyle('C'. $rowCnt)
                ->getNumberFormat()
                ->setFormatCode(self::FORMAT_CURRENCY_USD_SIMPLE);

            $spreadsheet->getActiveSheet()
                ->getStyle('D'. $rowCnt)
                ->getNumberFormat()
                ->setFormatCode(self::FORMAT_CURRENCY_USD_SIMPLE);

            $spreadsheet->getActiveSheet()
                ->getStyle('E'. $rowCnt)
                ->getNumberFormat()
                ->setFormatCode(self::FORMAT_CURRENCY_USD_SIMPLE);

            //increment cnt    
            $rowCnt++;
        }


        $this->outputHeaders($spreadsheet);
    }

    private function outputHeaders(Spreadsheet $spreadsheet)
    {
        //create download
        $writer = new Xlsx($spreadsheet);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="flaviar.xlsx"');
        header('Cache-Control: max-age=0');
        header('Expires: Fri, 11 Nov 2011 11:11:11 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        
        $writer->save('php://output');
    }

    public function cachedDataByDate($date = "")
    {
        $rate = Rate::where('full_date', $date)->first();

        $jsonOut = json_encode(["err" => "No exchange rate for the choosen date"]);

        if(isset($rate))
            $jsonOut = json_encode(["rate" => $rate->rate]);

        die($jsonOut);
    }
}